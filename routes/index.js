var express = require("express");
const path = require('path');
var Router = express.Router();

Router.get("/say-hello", function(req, res, next) {
  res.status(200).send({ message: "hello world" });
});


Router.use('/',require('./user/index.router.js'));
Router.use('/',require('./authenticate/authenticate.js'));

module.exports = Router;
